(ns tsp.main
  (:require [tsp.util.file-util :as file-util]
            [tsp.util.math-util :as math-util]
            [clojure.set :as cloj-set]
            [clojure.contrib.command-line :as ccl]
            )
  (:gen-class))

(defn swap [tour firstCityIndex secondCityIndex]
  "Swaps two routes in a tour"
  (if
    (not= firstCityIndex secondCityIndex)
    (let [tourVector (into [] tour)
          actualFirst (if (< firstCityIndex secondCityIndex) firstCityIndex secondCityIndex)
          actualSecond (if (= actualFirst firstCityIndex) secondCityIndex firstCityIndex)
          firstSection (subvec tourVector 0 actualFirst)
          swapSection (subvec tourVector actualFirst actualSecond)
          restSection (subvec tourVector actualSecond)
          ]
      (flatten (conj firstSection (reverse swapSection) restSection)))
    (do #(println (str "firstCity: " firstCityIndex " secondCity: " secondCityIndex)) (into [] tour))
    ))

(defn createGreedyTour [cityList]
  "Creates a greedy tour from the first city in the list"
  (letfn [(greedyTourAcc [nodes tour]
            (if (empty? nodes)
              (conj tour (first tour))
              (let [currentNode (last tour)
                    distanceMap (zipmap nodes (pmap (fn [x] (math-util/estimate-distance currentNode x cityList)) nodes))
                    nextNode (first (pmap key (select-keys distanceMap (for [[k v] distanceMap :when (= v (apply min (vals distanceMap)))] k))))
                    nextNodes (cloj-set/difference (into #{} nodes) #{nextNode})]
                (recur nextNodes (conj tour nextNode))
                ) ;end let
              ))]
    (greedyTourAcc (rest (into #{} (range (count cityList)))) (vector 0))))

(defn twoOptSwap [initialTour cityCoordinates]
  "Iterates over tour and performs improving 2-opt swaps until no more improvements are found or all possible swaps have been performed."
  (letfn [(twoOptMech [currentTour improvement]
            #(println (str "currentTour: " (into [] currentTour) " improvement: " improvement))
            (if (not improvement) currentTour
              (let [tourSize (count currentTour)
                    bestDistance (math-util/estimateTourDistance currentTour cityCoordinates)
                    candidates
                    (for [i (range 1 (- tourSize 2))
                          j (range 2 (- tourSize 2))
                          :when (< (math-util/estimateTourDistance (swap currentTour i j) cityCoordinates) bestDistance)]
                      (swap currentTour i j))
                    candidateTour (do #(println (str "candidates: " candidates " currentTour: " (into [] currentTour))) (if (nil? (not-empty candidates)) currentTour (nth candidates 0)))
                    nextDistance (math-util/estimateTourDistance candidateTour cityCoordinates)]
                (if (and (> nextDistance 0) (< nextDistance bestDistance)) (recur candidateTour true) (recur currentTour false)))
              )
            )]
    (twoOptMech initialTour true)))

(defn constructGreedyFromDataset [dataset]
  "Using the provided datasetFilename a greedy tour is constructed"
  (createGreedyTour (file-util/parseCoordinates dataset)))

(defn -main [& args]
  (ccl/with-command-line args
    "Main function to receive input and construct tours"
    [[dataset "The dataset to construct a tour from. The first line consisting of the number of cities. Subsequent lines should be X,Y coordinates for each city."]]
    (let [cityList (file-util/parseCoordinates dataset)
          tour (createGreedyTour cityList)]
      (println "Tour:" tour)
      (println "Total distance: " (math-util/computeTourDistance tour cityList)))))
