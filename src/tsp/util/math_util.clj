(ns tsp.util.math-util
  (:require [clojure.contrib.math :as cloj-math]
            [clojure.contrib.generic.math-functions :as math-gen]
            )
  (:gen-class))

(defn square [base]
  "Computes the square of a base"
  (cloj-math/expt base 2))

(defn distance-nonmemo [firstCity secondCity cityCoordinateList]
  "Calculates the euclidean distance from two cities (expected to be indices) in the provided city coordinate list"
  (let [firstCoord (nth cityCoordinateList firstCity)
        secondCoord (nth cityCoordinateList secondCity)]
    (math-gen/sqrt
      (+ (square (cloj-math/abs (- (:x firstCoord) (:x secondCoord))))
        (square (cloj-math/abs (- (:y firstCoord) (:y secondCoord)))))
      )))

(defn manhatten-distance [firstCity secondCity cityCoordinateList]
  (let [firstCoord (nth cityCoordinateList firstCity)
        secondCoord (nth cityCoordinateList secondCity)]
    (+ (cloj-math/abs (- (:x firstCoord) (:x secondCoord))) (cloj-math/abs (- (:y firstCoord) (:y secondCoord))))
    ))

(def distance (memoize distance-nonmemo))

(def estimate-distance (memoize manhatten-distance))

(defn computeTourDistance [tour cityList]
  "Computes the tour euclidean distance given the tour and list of city coordinates"
  (apply + (pmap (fn [x] (distance (first x) (second x) cityList)) (partition 2 1 tour)))
  )

(defn estimateTourDistance [tour cityList]
  "Computes the tour manhattan distance given the tour and list of city coordinates"
  (apply + (pmap (fn [x] (estimate-distance (first x) (second x) cityList)) (partition 2 1 tour)))
  )
