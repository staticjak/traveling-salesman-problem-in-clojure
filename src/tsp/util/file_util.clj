(ns tsp.util.file-util
  (:require [clojure.string :as cloj-str]
            )
  (:gen-class))

(defn createNodeLines
  "Creates a list of vectors containing the coordinates as strings from raw file contents"
  [unParsedNodeLines]
  (map #(cloj-str/split % #"\s") (rest (cloj-str/split-lines unParsedNodeLines)))
  )

(defn readFile [fileName]
  (slurp fileName))

(defn parseCoordinates [dataset]
  "Creates a list of maps which contain x, y coordinates for each city"
  (let [nodeLines (createNodeLines (readFile dataset))]
    into '() (map #(zipmap [:x :y] %) (map #(map read-string %) nodeLines))))