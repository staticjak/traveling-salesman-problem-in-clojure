## Traveling Salesman Problem Solver in Clojure!

This project uses the leiningen plugin for build automation and dependency management. Visit [Leiningen](http://www.leiningen.org) for more information.

### Usage

This project contains several datasets located in the data directory. In order to construct a tour with a dataset you can
use the lein command as follows (with any of the dataset names in the data directory):

    lein run -dataset 'data/tsp_5_1'

If you build a jar, you can use provide the same argument in the java command you use to run the jar as follows.

    java -jar tsp-0.1-standalone.jar -dataset 'data/tsp_5_1'