(defproject tsp "0.1"
  :dependencies [[org.clojure/clojure
                  "1.5.1"]
                 [org.clojure/clojure-contrib
                  "1.2.0"]
                 [org.clojure/tools.namespace "0.2.4"]]
  :main tsp.main)

