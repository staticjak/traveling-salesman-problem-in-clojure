(ns test_tsp)

(use 'clojure.test)
(use 'tsp)

(deftest createGreedyTourTest
  (def cityList '({:y 0, :x 0} {:y 0.5, :x 0} {:y 1, :x 0} {:y 1, :x 1} {:y 0, :x 1}))
  (is (= [0 1 2 3 4] (createGreedyTour cityList))))